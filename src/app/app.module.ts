import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import esCh from '@angular/common/locales/de-CH';
import ptPt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule, MatButtonModule, MatCardModule, MatDialogModule, MatExpansionModule, MatFormFieldModule, MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatRippleModule, MatSelectModule, MatSidenavModule, MatSnackBarModule, MatToolbarModule, MatTooltipModule, MatProgressBar, MatProgressBarModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularResizedEventModule } from 'angular-resize-event';
import { ChartsModule } from 'ng2-charts';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartComponent } from './custom-components/chart/chart.component';
import { DialogComponent } from './custom-components/dialog/dialog.component';
import { FastInfoComponent } from './custom-components/fast-info/fast-info.component';
import { InputComponent } from './custom-components/input/input.component';
import { ListItemMenuLeftComponent } from './custom-components/menu/list-item-menu-left/list-item-menu-left.component';
import { MenuLeftComponent } from './custom-components/menu/menu-left/menu-left.component';
import { MenuRightComponent } from './custom-components/menu/menu-right/menu-right.component';
import { HeadTableComponent } from './custom-components/table/head-table/head-table.component';
import { TableComponent } from './custom-components/table/table.component';
import { ContactTypeCadComponent } from './pages/contact-type/contact-type-cad/contact-type-cad.component';
import { ContactTypeComponent } from './pages/contact-type/contact-type.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthInterceptor } from './services/auth.interceptor';
import { ConfigService } from './services/config.service';
import { TranslateModule } from './utils/translate/translate.module';
import { NgxUiLoaderModule, NgxUiLoaderRouterModule, NgxUiLoaderHttpModule, NgxUiLoaderConfig, POSITION, SPINNER, PB_DIRECTION  } from  'ngx-ui-loader';
import { ngxUiLoaderConfig } from './utils/values';

registerLocaleData(esCh, "es-CH");
registerLocaleData(ptPt, "pt");



@NgModule({
  declarations: [
    AppComponent,
    //Cumtom component
    InputComponent,
    MenuRightComponent,
    MenuLeftComponent,
    HeadTableComponent,
    TableComponent,
    DialogComponent,
    ListItemMenuLeftComponent,
    ChartComponent,
    //Paginas
    HomeComponent,
    LoginComponent,
    ContactTypeComponent,
    ContactTypeCadComponent,
    FastInfoComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ChartsModule,
    AngularResizedEventModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    NgxUiLoaderRouterModule,
    NgxUiLoaderHttpModule,
    //Modulos Internos
    TranslateModule,    
    //Modulos do Material Design
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatExpansionModule,
    MatListModule,
    MatSidenavModule,
    MatMenuModule,
    MatTooltipModule,
    MatBadgeModule,
    MatToolbarModule,
    MatRippleModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatDialogModule,
    MatSnackBarModule,
    MatGridListModule,
    MatProgressBarModule
  ],
  exports: [
    //Modulos do Material Design
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatExpansionModule,
    MatListModule,
    MatSidenavModule,
    MatMenuModule,
    MatTooltipModule,
    MatBadgeModule,
    MatToolbarModule,
    MatRippleModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatDialogModule,
    MatSnackBarModule,
    MatGridListModule,
    MatProgressBarModule
  ],
  entryComponents: [
    DialogComponent,
    ContactTypeCadComponent
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    AppComponent,
    ConfigService,
    {
      provide: LOCALE_ID,
      deps:[ConfigService],
      useFactory: (config: ConfigService) => config.getLocale()
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
