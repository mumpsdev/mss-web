import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { ngIfFade, ngIfFadeOut} from './utils/animates.custons';
import { TranslateService } from './utils/translate/translate.service';
import { secretToken } from './utils/values';
import { Location } from '@angular/common';
import { UtilsService } from './services/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ngIfFade, ngIfFadeOut]
})
export class AppComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;
  private isPagesRoot: boolean = true;
  private pagesRoot = ["/", "/home"];
  private title: string;
  private showRoutes: boolean = true;
  private user = {};
  private isLogged: boolean = false;
  private links: Array<any> = [];
  public menuActions = [];
  public fixedMenuActions = [];
  public showMenuSide: boolean = false;
  private scrollTop = 0;
  public scrollUp: boolean = false;
  private scrollingPage = false;
  public isPageLoad;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private location: Location,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {
    this.setLinksTemp();
    if (!this.user["name"]) {
      // this.logout();
    }
    this.isLogged = true;
    this.setLinksTemp();
    this.links = this.createLinks(this.user)
  }

  @HostListener('window:scroll', ['$event'])
  onScrollPage(event) {
    if (!this.scrollingPage) {
      this.scrollingPage = true;
      setTimeout(() => {
        let scrollTop = event.target.scrollingElement.scrollTop | event.scrollY;
        if (scrollTop > 10 && scrollTop > this.scrollTop) {
          this.scrollUp = true;
        } else {
          this.scrollUp = false;
        }
        this.scrollTop = scrollTop;
        this.scrollingPage = false;
      }, 300);
    }
    this.utilsService.scrollY.next(event.target.scrollingElement.scrollTop)
    this.utilsService.scrollX.next(event.target.scrollingElement.scrollLeft)
  }

  public setUser(obj: any) {
    let user = obj["user"];
    if (obj[secretToken.TOKEN] && obj[secretToken.TOKEN] != "undefined") {
      sessionStorage.setItem(secretToken.TOKEN, obj[secretToken.TOKEN]);
      this.router.navigate(['/home']);
    }
    if (user) {
      this.user = user;
      this.setLinksTemp();
      this.links = this.createLinks(user)
      this.isLogged = true;
    }
  }

  setLinksTemp() {
    this.user["profiles"] = [
      {
        "actions": [
          { "name": "Dashboard", "router": "/home", "icon": "home", "selected": true, "isMenu": true},
          { "name": "Tipo de contato", "group": "Cadastro", "router": "/contact-type", "icon": "android", "isMenu": true},
          { "name": "Tipo de contato 2", "group": "Cadastro", "router": "/contact-type", "icon": "description", "isMenu": true},
          { "name": "Tipo de contato 3", "group": "Cadastro", "router": "/contact-type", "icon": "delete", "isMenu": true},
          { "name": "Grupo teste 2 1", "group": "Teste 2", "router": "/contact-type", "icon": "payment", "isMenu": true},
          { "name": "Grupo teste 2 2", "group": "Teste 2", "router": "/contact-type", "icon": "pets", "isMenu": true},
          { "name": "Grupo teste 2 2", "group": "Teste 2", "router": "/contact-type", "icon": "pan_tool", "isMenu": true},
          { "name": "Link 1", "router": "/contact-type", "icon": "list", "isMenu": true},
          { "name": "Link 2", "router": "/contact-type", "icon": "face", "isMenu": true},
          { "name": "Link 3", "router": "/contact-type", "icon": "dns", "isMenu": true},
          { "name": "Link 4", "router": "/contact-type", "icon": "https", "isMenu": true}
        ]
      }
    ];
  }

  close() {
    this.showMenuSide = false;
  }

  public logout() {
    this.user = {};
    sessionStorage.removeItem(secretToken.TOKEN);
    this.clearMenuActions();
    this.clearFixedMenuActions();
    this.isLogged = false;
    this.router.navigate(["/login"])
  }

  public setTittlePage(title: string) {
    setTimeout(() => { this.title = "" }, 100);
    setTimeout(() => {
      let transTitle = this.translate.getValue(title);
      this.title = transTitle
    }, 500);
  }

  public getLoadBar(): boolean {
    return true;
  }

  public setShowRoutes(show: boolean) {
    this.showRoutes = show;
  }


  public createLinks(user: any): Array<any> {
    let links = [];
    let profiles = user["profiles"];
    if (profiles) {
      profiles.forEach(profile => {
        let actions = profile["actions"];
        if (actions) {
          actions.forEach(action => {
            if (action["group"]) {
              let group = links.find((l) => l["group"] == action["group"]);
              if (group) {
                group["links"].push({ "name": action["name"], "router": action["router"], "icon": action["icon"] })
              } else {
                links.push({ "group": action["group"], "links": [{ "name": action["name"], "router": action["router"], "icon": action["icon"] }] });
              }
            } else if (action["router"]) {
              links.push({ "name": action["name"], "router": action["router"], "icon": action["icon"] });
            }
          });
        }
      });
    }
    return links;
  }
  /**
   * Método que é chamado toda vez que a página é carregada.
   * @param page Recebe a rota atual.
   */
  public routerActived(page) {
    let urlNow = this.router.url;
    this.setRouterNow(this.links, urlNow);

    // this.isPagesRoot = false;
    // if(this.pagesRoot.some( page => page == urlNow)){
    //   this.isPagesRoot = true;
    // }
    this.clearMenuActions();
  }

  public setRouterNow(list: Array<any>, routerNow) {
    let routerSelected: boolean = false;
    list.forEach((link) => {
      if (link.router && link.router == routerNow && !routerSelected) {
        routerSelected = true;
        link["selected"] = routerSelected;
      } else if (link.links && !routerSelected) {
        this.setRouterNow(link.links, routerNow);
      }
    });
  }

  public backPage() {
    this.location.back();
  }

  public clearMenuActions() {
    this.menuActions = [];
  }

  public clearFixedMenuActions() {
    this.fixedMenuActions = [];
  }

  public clickAction(action: any) {
    if (action.action) action.action();
    if (action.router) this.router.navigate([action.router]);
    if (action.notRead) {
      action.notRead = undefined;
    }
    let idAction = action["idAction"];
    if (idAction) {
      let actionMaster = this.fixedMenuActions ? this.fixedMenuActions.find(f => f.id == idAction) : undefined;
      if (!actionMaster) {
        actionMaster = this.menuActions ? this.menuActions.find(f => f.id = idAction) : undefined;
      }
      if (actionMaster && actionMaster["links"]) {
        let notHead = actionMaster["links"].filter((l) => l.notRead);
        actionMaster["badge"] = notHead.length;
      }
    }
  }

  public addFixedAction(id: string, icon: string, callBack?: any, label?: string, badge?: string) {
    let notCan = this.fixedMenuActions.some((a) => {
      return a.id == id
    });
    if (!notCan) {
      let action = {"id": id, "icon": icon, "action": callBack, "label": label, "badge": badge};
      setTimeout(() => {
        this.fixedMenuActions.push(action);
      }, 100);
    }
  }

  public addAction(id: string, icon: string, callBack?: any, label?: string, badge?: string) {
    let notCan = this.menuActions.some((a) => {
      return a.id == id
    });
    if (!notCan) {
      let action = {"id": id, "icon": icon, "action": callBack, "label": label, "badge": badge};
      setTimeout(() => {
        this.menuActions.push(action);
      }, 100);
    }
  }

  public removeFixedAction(id: any) {
    this.fixedMenuActions = this.fixedMenuActions.filter((action) => {
      return action.id != id;
    });
  }

  public removeAction(id: any) {
    this.menuActions = this.menuActions.filter((action) => {
      return action.id != id;
    });
  }

  public addLinkFixedInAction(idAction: string, id: string, icon: string, router?: string, callBack?: any, label?: string, badge?: string) {
    let actionMenu = this.fixedMenuActions.find(e => e.id == idAction);
    if (actionMenu) {
      if (!actionMenu["links"]) {
        actionMenu["links"] = [];
      }
      let action = {"id": id, "icon": icon, "router": router, "action": callBack, "label": label, "badge": badge, "idAction": idAction};
      actionMenu.links.push(action);
      let notHead = actionMenu.links.filter((l) => l.notRead);
      actionMenu["badge"] = notHead.length;
    }
  }

  public addLinkInAction(idAction: string, id: string, icon: string, router?: string, callBack?: any, label?: string, badge?: string) {
    let actionMenu = this.menuActions.find(e => e.id == id);
    if (actionMenu) {
      if (!actionMenu["links"]) {
        actionMenu["links"] = [];
      }
      let action = {"id": id, "icon": icon, "router": router, "action": callBack, "label": label, "badge": badge, "idAction": idAction};
      actionMenu.links.push(action);
      let notHead = actionMenu.links.filter((l) => l.notRead);
      actionMenu["badge"] = notHead.length;
    }
  }

  public havePermission(url: string, type: string): boolean {
    let have: boolean = false;
    let perfis: any = this.user["perfis"];
    if (perfis) {
      perfis.forEach(perfil => {
        let acoes = perfil["acoes"];
        if (acoes) {
          acoes.forEach(acao => {
            if (acao.url == url && acao.tipo == type) {
              have = true;
            }
          });
        }
      });
    }
    return have;
  }
}
