import { secretToken } from './../utils/values';
import { Injectable, OnInit } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router"
import { Observable } from "rxjs";
import { AppComponent } from '../app.component';



@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private app: AppComponent
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,

  ): Observable<boolean> | boolean {
    let token = sessionStorage.getItem(secretToken.TOKEN);
    // if (!token || token == "undefined") {
    //   this.app.logout();
    //   return false;
    // }
    return true;
  }
}
