import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import * as cryptojs from "crypto-js";
import { Subject } from 'rxjs';
import { DialogComponent } from '../custom-components/dialog/dialog.component';
import { LANG_PT_TRANS } from '../utils/translate/lang-pt';
import Validation from '../utils/Validation';
import { TranslateService } from './../utils/translate/translate.service';
import { objectMsg, secretToken, typeMsg } from './../utils/values';

@Injectable({
    providedIn: "root"
  })
export class UtilsService {
    public scrollY = new Subject();
    public scrollX = new Subject();
    public reservedWord = ["page", "limit", "asc", "desc"]

    constructor(
        private translate: TranslateService,
        private dialog: MatDialog,
        private snackBar: MatSnackBar 
    ) { 
    }

    public hMacSha512(text: string):string{
        try {
            let hash = cryptojs.HmacSHA512(text, secretToken.SECRET_PUBLIC);
            return hash.toString();
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public encrypt(text: string):string{
        try {
            let ciphertext = cryptojs.AES.encrypt(JSON.stringify(text), secretToken.SECRET_PUBLIC).toString();
            return ciphertext;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public decrypt(ciphertext: any):string{
        try {
            let bytes  = cryptojs.AES.decrypt(ciphertext.toString(), secretToken.SECRET_PUBLIC);
            var decryptedData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));
            return decryptedData;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + ciphertext);
            return "";
        }
    }

    public getOnlyNumbers(value){
        if(value){
            try {
                return value.replace(/[^0-9]+/g, "");
            } catch (e) {console.log(e);}
        }
        return null;
    };
  
    public getMsgStatusError(status){
        let msg: string = this.translate.getValue("statusDefault");
        switch (status) {
            case 0: msg = this.translate.getValue("status0"); break;
            case 302: msg = this.translate.getValue("status302"); break;
            case 304: msg = this.translate.getValue("status304"); break;
            case 400: msg = this.translate.getValue("status400"); break;
            case 401: msg = this.translate.getValue("status401"); break;
            case 403: msg = this.translate.getValue("status403"); break;
            case 404: msg = this.translate.getValue("status404"); break;
            case 405: msg = this.translate.getValue("status405"); break;
            case 410: msg = this.translate.getValue("status410"); break;
            case 500: msg = this.translate.getValue("status500"); break;
            case 502: msg = this.translate.getValue("status302"); break;
            case 503: msg = this.translate.getValue("status302"); break;
            default: break;
        }
        return msg;
        
    };
  
    public getItemList = (lista, value, campo) => {
        if(!campo){campo = "id"};
        var item = null;
        if(value && lista){
            for(var index = 0; index < lista.length; index++){
                if(lista[index][campo] == value){
                    item = lista[index];
                    break;
                }
            }
        }
        return item;
    };

    public getCountDuplicateList = (lista, value, campo) => {
        let count = 0;
        if(!campo){campo = "id"};
        var item = null;
        if(value && lista){
            for(var index = 0; index < lista.length; index++){
                if(lista[index][campo] == value){
                    count++;
                }
            }
        }
        return count;
    };
  
    public removeItemList = (lista, value, campo) =>{
        if(!campo){campo = "id"};      
        var item = null;
        if(value && lista){
            for(var index = 0; index < lista.length; index++){
                if(lista[index][campo] == value){
                    lista.splice(index, 1);
                    break;
                }
            }
        }
    };
  
    public filterItemList = (lista, value:string, campo:string) => {
        if(!campo){campo = "id"};
        if(!value) return lista;
        return lista.filter(item => { 
            return  item[campo].toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) != -1;
        });
    };

    public changeItemList(oldList: Array<any>, newList: Array<any>, item: any){
        if(oldList && newList){
          let index = oldList.indexOf(item);
          if(index > -1){
            oldList.splice(index, 1);
          }
          newList.push(item);
        }
    }

    public getValueObjectField(obj: object, field: string){
        if(field.includes(".")){
            let fields = field.split(".");
            fields.forEach((fieldSplit) => {
              obj = obj[fieldSplit];
            });
            return obj;
        }
        return obj[field] || "";
    }
       
    public createValidator(func: any, msg: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: string } | null => {
            if (!func(control.value)) {
                msg = this.translate.getValue(msg);
                return { 'msgErro': msg };
            }
            return null;
        };
    }

    public  getFilterPagination(filter){
        let newFilter: string = "?";
        let page = (filter.page) ? "page=" +  filter.page:  "&page=1";
        let limit = (filter.limit) ? "&limit=" +  filter.limit :  "&limit=10";
        let asc = (filter.asc) ? "&asc=" +  filter.asc :  "";
        let desc = (filter.desc) ? "&desc=" +  filter.desc :  "";
        newFilter += page + limit + asc + desc;
        return newFilter;
    }

    async showDialog(title: string, msg: string, buttons?: Array<any>, icon?: string, width?: string){
        if(!title) title = "MENSAGEM";
        let data = {
            "title": title,
            "msg" : msg,
            "btns": buttons,
            "icon": icon,
            "width": width
        }
        let dialogRef = this.dialog.open(DialogComponent, {"data": data});
        return dialogRef;
    }

    async toask(msg: string, labelAction?: string, duration?: number){
        let options = {};
        if(!msg) {msg = this.translate.getValue(LANG_PT_TRANS.MESSAGE)}
        if(!labelAction) {labelAction = ""}
        if(!duration){duration = 3000;}
        let snack = await this.snackBar.open(msg, labelAction, {
            "duration": duration,
        });
    }

    async showMesseges(data: any): Promise<string>{
        let msgErros = "";
        if(data){
            let listMsgs = [];
            if(data instanceof HttpErrorResponse){
                listMsgs = data[objectMsg.ERROR][objectMsg.LIST_MSG];
            }else{
                listMsgs = data[objectMsg.LIST_MSG];
            }
            if(listMsgs){
                listMsgs.forEach(async message => {
                    if(message.type && message.type == typeMsg.DANGER){
                        if(message.msg){
                            msgErros += this.translate.getValue(message.msg) + "\n";
                        }
                    }else if(message.msg){
                       let toask = await this.toask( this.translate.getValue(message.msg));
                    }
                });
            }else if(data instanceof HttpErrorResponse){
                let status = data[objectMsg.STATUS];
                if(status >= 0){
                    console.log(`Status: ${status}`);
                    msgErros += this.translate.getValue("status" + status)
                }
            }
            if(msgErros != null && msgErros.length > 0){
                let dialog: any;
                let buttonOk = {"text": this.translate.getValue(LANG_PT_TRANS.OK), "icon": "check", "close": true}
                dialog = await this.showDialog(null, msgErros, [buttonOk], "warning");
                console.log('Confirm Cancel');
            }
        }
        return msgErros;
    }

    setValueStorege(key: string, value: string){
       sessionStorage.setItem(key, value);
    }

    getValueStorege(key: string): any {
        return sessionStorage.getItem(key)
    }

    removeValueStorege(key: string){
         sessionStorage.removeItem(key);
    }

    createFiltersList(list: any): string{
        let filters = "";
        for(var key in list){
            if(!Validation.isOnlyNumbers(list[key]) || this.reservedWord.includes(key)){
                filters +=  key  + "="+  list[key] + "&";
            }else{
                filters +=  key + "=%" + list[key] + "%&";
            }
        }
        return filters;
    }

    concatObjects(sourceObject: any, destinyObject: any): any{
        for(let key in sourceObject){
            destinyObject[key] = sourceObject[key];
        }
        return destinyObject;
    }

    public isPlatformMobile(): boolean {
        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return true;
        }
        else {
            return false;
        }
    }
}