import { Component, OnInit} from '@angular/core';
import { AppComponent } from '../../app.component';
import {FormControl, FormGroup} from '@angular/forms'
import { UtilsService } from '../../services/utils.service';
import Validation from '../../utils/Validation';
import { ngIfFade } from '../../utils/animates.custons';
import { LANG_PT_TRANS } from '../../utils/translate/lang-pt';
import { URLs, objectMsg, secretToken } from '../../utils/values';
import { TranslateService } from '../../utils/translate/translate.service';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { tap, map, take } from 'rxjs/operators';

@Component({
  selector: 'm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [ngIfFade]
})
export class LoginComponent implements OnInit {
  private loginFocus: boolean = false;
  user = {};

  formLogin: FormGroup;
  loginControl = new FormControl('', [
    this.UtilsService.createValidator(Validation.isEmpty, LANG_PT_TRANS.REQUIRED_FIELD),
  ]);
  passwordControl = new FormControl('', [
    this.UtilsService.createValidator(Validation.isEmpty, LANG_PT_TRANS.REQUIRED_FIELD),
  ]);

  constructor(
    private app: AppComponent,
    private UtilsService: UtilsService,
    private translate: TranslateService,
    private router: Router,
    private loginService: LoginService
  ) { 
    this.formLogin = new FormGroup({
      "login": this.loginControl,
      "password": this.passwordControl,
      
    });
  }

  ngOnInit() {
    this.app.logout();
  }
  
  public proximo(){
    this.loginService.getUserNameUserLogged(this.loginControl.value)
    .pipe(
        map(data => data[objectMsg.OBJ]),
        tap(u => u ? console.log(u) : console.log("Obj null"),
        take(0),
      )
    ).subscribe(user => {
      this.user = user;
      this.loginFocus = false;
    }, erro => {
      let msg = this.translate.getValue(LANG_PT_TRANS.LOGIN_INVALID);
      this.loginControl.setErrors({"msgErro": msg});
      this.loginControl.markAsDirty()
    });
  }
  
  public login(){
    this.user["password"] = this.passwordControl.value;
    this.loginService.login(this.formLogin.value)
    .pipe(
      map(data => data[objectMsg.OBJ]),
      tap(u => console.log(u),
      take(0)
      )
    ).subscribe(user => {
      this.app.setUser(user);
    }, erro => {
      this.passwordControl.markAsTouched();
      let msg = this.translate.getValue(LANG_PT_TRANS.LOGIN_PASSWORD_INVALID);
      this.passwordControl.setErrors({"msgErro": msg});
    })
  }

  public voltar(){
    this.user = {};
  }

  public teste(event){
    console.log("Chamou o teste");
    
    console.log(event);
  }

  public testeDialog(){
    let dialogRef = this.UtilsService.showDialog(null, "Mensagem de teste do dialog",
     [{"text": "Voltar", "icon": "chevron_left", "close": true, "action": () => {
       this.voltar();
     }}], null, null);

  }
}

