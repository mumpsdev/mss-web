import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactTypeCadComponent } from './contact-type-cad.component';

describe('ContactTypeCadComponent', () => {
  let component: ContactTypeCadComponent;
  let fixture: ComponentFixture<ContactTypeCadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactTypeCadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactTypeCadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
