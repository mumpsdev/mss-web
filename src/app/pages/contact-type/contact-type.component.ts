import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { UtilsService } from 'src/app/services/utils.service';
import { ContactTypeService } from 'src/app/services/contact.type.service';
import { objectMsg } from 'src/app/utils/values';
import { take, tap, concatMap } from 'rxjs/operators';
import { ContactTypeCadComponent } from './contact-type-cad/contact-type-cad.component';
import { MatDialog } from '@angular/material';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';
import { async } from '@angular/core/testing';
import { ngIfScaleIn, ngIfScaleOut,  } from 'src/app/utils/animates.custons';
import { delay } from 'q';
import { of } from 'rxjs';

@Component({
  selector: 'app-contact-type',
  templateUrl: './contact-type.component.html',
  styleUrls: ['./contact-type.component.scss'],
  animations:[ngIfScaleIn, ngIfScaleOut]
})
export class ContactTypeComponent implements OnInit {

  private dataContactType: any;
  private selectedContactType: any;
  private listFilters = {"page": 1, "limit": 10, "name": ""};
  showFilter: boolean = false;


  constructor(
    private app: AppComponent,
    private translate: TranslateService,
    private utilsService: UtilsService,
    private contactTypeService: ContactTypeService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    let title  = this.translate.getValue("CONTACT_TYPE");
    this.app.setTittlePage(title);
    this.app.addAction("searchCaontactType", "search", ()=> {
      this.showFilter = !this.showFilter;
    })
    this.listContactTypes();
  }

  async listContactTypes(){
    let filters = this.utilsService.createFiltersList(this.listFilters);
    console.log(filters);
    this.contactTypeService.list(filters).pipe(take(1)).subscribe(async data => {
      await delay(10000);
      this.dataContactType = data;
      console.log(this.dataContactType);
      await this.utilsService.showMesseges(data);
    }, error => this.utilsService.showMesseges(error));
  }

  async updateTable(filterTable){
    console.log(filterTable);
    this.listFilters = this.utilsService.concatObjects(filterTable, this.listFilters);
    console.log(this.listFilters);
    await this.listContactTypes();
  }

  async clearFilter(){
    this.listFilters.name = "";
    this.listContactTypes();
  }

  async showCadContactType(contactType){
    console.log(contactType);
    let dialogRef = this.dialog.open(ContactTypeCadComponent, {
      disableClose: true,
      width: "30em",
      maxWidth: "90%",
      position: {"top": "1rem"},
      data: contactType
    });
    dialogRef.afterClosed().subscribe(resultado => {
      if(resultado == "ok"){
        this.listContactTypes();
      }
    });
  }

  async showRemove(contactType){
    let btnYes = {"icon": "delete_forever", "text": LANG_PT_TRANS.YES, "close": true, "action": async () =>{
      await this.remove(contactType.id);
      this.listContactTypes();
    }};
    let btnCancel = {"icon": "cancel", "text": "CANCEL", "close": true};
    let msg = this.translate.getValue(LANG_PT_TRANS.WANT_REMOVE, contactType.name);
    this.utilsService.showDialog(null, msg, [btnCancel, btnYes], "warning");
  }

  async remove(id){
    this.contactTypeService.delete(id).pipe(take(1)).subscribe(async data =>{
      await this.utilsService.showMesseges(data);
      this.listContactTypes();
    }, error => this.utilsService.showMesseges(error))
  }

}
