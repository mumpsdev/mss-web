import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'm-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  private index = 0;
  private open = true;

  private list1 = [
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Rogerio Brito de Oliveira", "age": 32, "birthDayMonth": "Abril", "workinogYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Carlos Eduardo", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Sophie Vitório Andrade de Oliveira", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Eduardo 1", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Eduardo 2", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Eduardo 3", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Eduardo 4", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Eduardo 5", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Eduardo 6", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00}
  ];
  private list2 = [
    {"name": "Carlos", "age": 2, "birthDayMonth": "Novembro", "workingYears": 5, "extraHour": 13, "baseSalary": 3100.00, "image": "https://www.ambienteenergia.com.br/wp-content/uploads/2016/03/energia-nuclear.jpg", "birthDate": new Date(1984, 11, 24)},
    {"name": "Marcelo", "age": 36, "birthDayMonth": "Abril", "workingYears": 11, "extraHour": 74, "baseSalary": 8500.00, "image": "https://mundoeducacao.bol.uol.com.br/upload/conteudo_legenda/92d170b0f17cfd668c2f9a69bae1cf72.jpg", "birthDate": new Date(1999, 2, 2)},
    {"name": "Gustavo", "age": 45, "birthDayMonth": "janeiro", "workingYears": 7, "extraHour": 38, "baseSalary": 5500.00, "image": "http://dynamox.net/wp-content/uploads/2018/04/usinas-de-etanol.jpg", "birthDate": new Date(2014, 1, 22)},
    {"name": "Daivid", "age": 89, "birthDayMonth": "Março", "workingYears": 1, "extraHour": 77, "baseSalary": 1500.00, "image": "https://www.fragmaq.com.br/wp-content/uploads/2015/08/1-usina-termoeletrica.jpg", "birthDate": new Date(1992, 10, 22)},
    {"name": "Eduardo", "age": 30, "birthDayMonth": "janeiro", "workingYears": 2, "extraHour": 19, "baseSalary": 2300.00, "image": "https://www.pensamentoverde.com.br/wp-content/uploads/2018/02/usina-4.jpg", "birthDate": new Date(1990, 6, 15)},
    {"name": "Eduardo 1", "age": 11, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 22, "baseSalary": 2500.00},
    {"name": "Eduardo 2", "age": 45, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 5, "baseSalary": 1500.00},
    {"name": "Eduardo 3", "age": 36, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 41, "baseSalary": 4500.00},
    {"name": "Eduardo 4", "age": 78, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 69, "baseSalary": 2500.00},
    {"name": "Eduardo 5", "age": 28, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 11, "baseSalary": 1500.00},
    {"name": "Eduardo 6", "age": 40, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 10, "baseSalary": 4500.00}
  ];

  private list3 = [
    {"name": "Carlos", "age": 36, "birthDayMonth": "Novembro", "workingYears": 5, "extraHour": 23, "baseSalary": 3100.00, "image": "https://www.ambienteenergia.com.br/wp-content/uploads/2016/03/energia-nuclear.jpg", "birthDate": new Date(1984, 11, 24)},
    {"name": "Marcelo", "age": 47, "birthDayMonth": "Abril", "workingYears": 11, "extraHour": 49, "baseSalary": 8500.00, "image": "https://mundoeducacao.bol.uol.com.br/upload/conteudo_legenda/92d170b0f17cfd668c2f9a69bae1cf72.jpg", "birthDate": new Date(1999, 2, 2)},
    {"name": "Gustavo", "age": 45, "birthDayMonth": "janeiro", "workingYears": 7, "extraHour": 9, "baseSalary": 5500.00, "image": "http://dynamox.net/wp-content/uploads/2018/04/usinas-de-etanol.jpg", "birthDate": new Date(2014, 1, 22)},
    {"name": "Daivid", "age": 22, "birthDayMonth": "Março", "workingYears": 1, "extraHour": 2, "baseSalary": 1500.00, "image": "https://www.fragmaq.com.br/wp-content/uploads/2015/08/1-usina-termoeletrica.jpg", "birthDate": new Date(1992, 10, 22)},
    {"name": "Eduardo", "age": 25, "birthDayMonth": "janeiro", "workingYears": 2, "extraHour": 11, "baseSalary": 2300.00, "image": "https://www.pensamentoverde.com.br/wp-content/uploads/2018/02/usina-4.jpg", "birthDate": new Date(1990, 6, 15)},
    {"name": "Eduardo 1", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 22, "baseSalary": 2500.00},
    {"name": "Eduardo 2", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 24, "baseSalary": 1500.00},
    {"name": "Eduardo 3", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 78, "baseSalary": 4500.00},
    {"name": "Eduardo 4", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 63, "baseSalary": 2500.00},
    {"name": "Eduardo 5", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 89, "baseSalary": 1500.00},
    {"name": "Eduardo 6", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 14, "baseSalary": 4500.00}
  ];

  private list4 = [
    {"name": "Carlos", "age": 11, "birthDayMonth": "Novembro", "workingYears": 5, "extraHour": 23, "baseSalary": 3100.00, "image": "https://www.ambienteenergia.com.br/wp-content/uploads/2016/03/energia-nuclear.jpg", "birthDate": new Date(1984, 11, 24)},
    {"name": "Marcelo", "age": 44, "birthDayMonth": "Abril", "workingYears": 11, "extraHour": 22, "baseSalary": 8500.00, "image": "https://mundoeducacao.bol.uol.com.br/upload/conteudo_legenda/92d170b0f17cfd668c2f9a69bae1cf72.jpg", "birthDate": new Date(1999, 2, 2)},
    {"name": "Gustavo", "age": 77, "birthDayMonth": "janeiro", "workingYears": 7, "extraHour": 26, "baseSalary": 5500.00, "image": "http://dynamox.net/wp-content/uploads/2018/04/usinas-de-etanol.jpg", "birthDate": new Date(2014, 1, 22)},
    {"name": "Daivid", "age": 33, "birthDayMonth": "Março", "workingYears": 1, "extraHour": 15, "baseSalary": 1500.00, "image": "https://www.fragmaq.com.br/wp-content/uploads/2015/08/1-usina-termoeletrica.jpg", "birthDate": new Date(1992, 10, 22)},
    {"name": "Eduardo", "age": 23, "birthDayMonth": "janeiro", "workingYears": 2, "extraHour": 22, "baseSalary": 2300.00, "image": "https://www.pensamentoverde.com.br/wp-content/uploads/2018/02/usina-4.jpg", "birthDate": new Date(1990, 6, 15)},
    {"name": "Eduardo 1", "age": 35, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 37, "baseSalary": 2500.00},
    {"name": "Eduardo 2", "age": 12, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 3, "baseSalary": 1500.00},
    {"name": "Eduardo 3", "age": 22, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 69, "baseSalary": 4500.00},
    {"name": "Eduardo 4", "age": 10, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 58, "baseSalary": 2500.00},
    {"name": "Eduardo 5", "age": 13, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 5, "baseSalary": 1500.00},
    {"name": "Eduardo 6", "age": 1, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 10, "baseSalary": 4500.00}
  ];

  private list5 = [
    {"name": "Carlos", "age": 69, "birthDayMonth": "Novembro", "workingYears": 5, "extraHour": 80, "baseSalary": 3100.00, "image": "https://www.ambienteenergia.com.br/wp-content/uploads/2016/03/energia-nuclear.jpg", "birthDate": new Date(1984, 11, 24)},
    {"name": "Marcelo", "age": 24, "birthDayMonth": "Abril", "workingYears": 11, "extraHour": 55, "baseSalary": 8500.00, "image": "https://mundoeducacao.bol.uol.com.br/upload/conteudo_legenda/92d170b0f17cfd668c2f9a69bae1cf72.jpg", "birthDate": new Date(1999, 2, 2)},
    {"name": "Gustavo", "age": 26, "birthDayMonth": "janeiro", "workingYears": 7, "extraHour": 40, "baseSalary": 5500.00, "image": "http://dynamox.net/wp-content/uploads/2018/04/usinas-de-etanol.jpg", "birthDate": new Date(2014, 1, 22)},
    {"name": "Daivid", "age": 22, "birthDayMonth": "Março", "workingYears": 1, "extraHour": 4, "baseSalary": 1500.00, "image": "https://www.fragmaq.com.br/wp-content/uploads/2015/08/1-usina-termoeletrica.jpg", "birthDate": new Date(1992, 10, 22)},
    {"name": "Eduardo", "age": 34, "birthDayMonth": "janeiro", "workingYears": 2, "extraHour": 33, "baseSalary": 2300.00, "image": "https://www.pensamentoverde.com.br/wp-content/uploads/2018/02/usina-4.jpg", "birthDate": new Date(1990, 6, 15)},
    {"name": "Eduardo 1", "age": 52, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Eduardo 2", "age": 56, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Eduardo 3", "age": 59, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Eduardo 4", "age": 30, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Eduardo 5", "age": 91, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Eduardo 6", "age": 92, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00}
  ];

  private list6 = [
    {"name": "Erick", "age": 29, "birthDayMonth": "Julho", "workingYears": 3, "extraHour": 100, "baseSalary": 3500.00},
    {"name": "Carol", "age": 42, "birthDayMonth": "Outubro", "workingYears": 7, "extraHour": 20, "baseSalary": 6500.00},
    {"name": "Adilson", "age": 26, "birthDayMonth": "Abril", "workingYears": 2, "extraHour": 15, "baseSalary": 4500.00},
    {"name": "Alyson", "age": 23, "birthDayMonth": "julho", "workingYears": 2, "extraHour": 64, "baseSalary": 3800.00},
    {"name": "Rosangela", "age": 39, "birthDayMonth": "Junho", "workingYears": 7, "extraHour": 120, "baseSalary": 6500.00},
    {"name": "Eduardo 1", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Eduardo 2", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Eduardo 3", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00},
    {"name": "Eduardo 4", "age": 32, "birthDayMonth": "Abril", "workingYears": 3, "extraHour": 32, "baseSalary": 2500.00},
    {"name": "Eduardo 5", "age": 19, "birthDayMonth": "Setembro", "workingYears": 1, "extraHour": 1, "baseSalary": 1500.00},
    {"name": "Eduardo 6", "age": 52, "birthDayMonth": "Agosto", "workingYears": 10, "extraHour": 48, "baseSalary": 4500.00}
  ];

  private list7 = [
    { "name": "Carlos", "age": 36, "birthDayMonth": "Novembro", "workingYears": 5, "extraHour": 80, "baseSalary": 3100.00, "image": "https://www.ambienteenergia.com.br/wp-content/uploads/2016/03/energia-nuclear.jpg", "birthDate": new Date(1984, 11, 24) },
    { "name": "Marcelo", "age": 47, "birthDayMonth": "Abril", "workingYears": 11, "extraHour": 55, "baseSalary": 8500.00, "image": "https://mundoeducacao.bol.uol.com.br/upload/conteudo_legenda/92d170b0f17cfd668c2f9a69bae1cf72.jpg", "birthDate": new Date(1999, 2, 2) },
    { "name": "Gustavo", "age": 45, "birthDayMonth": "janeiro", "workingYears": 7, "extraHour": 40, "baseSalary": 5500.00, "image": "http://dynamox.net/wp-content/uploads/2018/04/usinas-de-etanol.jpg", "birthDate": new Date(2014, 1, 22) },
    { "name": "Daivid", "age": 22, "birthDayMonth": "Março", "workingYears": 1, "extraHour": 4, "baseSalary": 1500.00, "image": "https://www.fragmaq.com.br/wp-content/uploads/2015/08/1-usina-termoeletrica.jpg", "birthDate": new Date(1992, 10, 22) },
    { "name": "Eduardo", "age": 25, "birthDayMonth": "janeiro", "workingYears": 2, "extraHour": 33, "baseSalary": 2300.00, "image": "https://www.pensamentoverde.com.br/wp-content/uploads/2018/02/usina-4.jpg", "birthDate": new Date(1990, 6, 15) },
  ]

dataPie: any;
dataLineBar: any;
dataBubble: any;
dataFastInfo1 = [{"data": [320, 180, 400, 2000, 1840]}];

labelsPie = [];
labelsLineBar = [];
labelsFastInfo1 = ["Julho", "Agosto", "Setembro", "Outubro", "Novembro"];

constructor(
  private app: AppComponent
) { }

ngOnInit() {
  this.app.setTittlePage("DASHBOARD");
  // this.addBtn();
  this.dataPie = [{ "data": this.list7.map(data => data["age"]), "label": "Pessoas" }];
  this.dataLineBar = [
    { "data": this.list2.map(data => data["age"]), "label": "Concluidas" },
    { "data": this.list3.map(data => data["age"]), "label": "Finalizadas" },
    { "data": this.list4.map(data => data["age"]), "label": "Em espera" },
    { "data": this.list5.map(data => data["age"]), "label": "Recuperação" }
  ];
  this.dataBubble = [];
  this.list2.forEach( ( item) => {
    this.dataBubble.push({data:[{x: item.age, y: item.extraHour, r: 5}], label: item.name});
  });

  this.labelsPie = this.list7.map(data => data["name"]);
  this.labelsLineBar = this.list2.map(data => data["name"]);

}
  
  public addBtn(){
  this.index++;
  let btnPesquisar = { id: "btnPesquisar" + this.index };
  btnPesquisar["icon"] = "android";
  btnPesquisar["tooltip"] = "Pesquisar teste";
  btnPesquisar["label"] = "Login";
  btnPesquisar["router"] = "/login"
  // this.app.addAction(btnPesquisar);

}
  public addBtnP(){
  this.index++;
  let btnPesquisar = { id: "btnPesquisar" };
  btnPesquisar["icon"] = "warning";
  btnPesquisar["tooltip"] = "Pesquisar teste";
  btnPesquisar["badge"] = 0;
  btnPesquisar["action"] = () => {
    console.log("Chamou a por páginas!");
  };
  // this.app.addFixedAction(btnPesquisar);

}
  
  public addSubLink(){
  this.index++;
  let vejaLogin = { id: "vejaLogin" + this.index };
  vejaLogin["icon"] = "android";
  vejaLogin["label"] = "Veja na página login!";
  vejaLogin["router"] = "/login";
  vejaLogin["notRead"] = true;

  // this.app.addFixedActionLink("btnPesquisar", vejaLogin);
}
l = true;
}