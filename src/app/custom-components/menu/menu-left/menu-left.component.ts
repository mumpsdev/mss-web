import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ngIfSlideInLeft, ngIfSlideInRight, ngIfSlideOutleft, ngIfSlideOutRight, ngIfFade, ngIfFadeOut, ngIfScaleIn, ngIfScaleOut } from '../../../utils/animates.custons';

@Component({
  selector: 'm-menu-left',
  templateUrl: './menu-left.component.html',
  styleUrls: ['./menu-left.component.scss'],
  animations: [ngIfSlideOutRight, ngIfSlideInRight, ngIfSlideInLeft, 
    ngIfSlideOutleft, ngIfFade, ngIfFadeOut, ngIfScaleIn, ngIfScaleOut]
})
export class MenuLeftComponent implements OnInit {

  constructor(
  ) { 
  }

  _isShow: boolean = false;
  @Input() titleClass: string;
  @Input() title: string;
  @Input() width: string;
  @Input() full: boolean;
  @Input() modal: boolean = false;
  @Input() dark: boolean = false;
  @Input() bgColor: string;
  @Input() bgImage: string;
  @Input() bgImageTop: string;
  @Output() close: EventEmitter<any> = new EventEmitter();
  
  
  private fixed: boolean = true;
  public isScroll: boolean = false;
  public animateContainer:any;
  public animateMove:any;
  
  ngOnInit() {
    console.log(this._isShow);
  }

  public closeClick(){
    this.isShow = false;
    this.close.emit(this.isShow);
  }

  @Input()
  set isShow(value: boolean){
    this.setValue(value);
  }

  get isShow(){
    return this._isShow;
  }

  setValue(value: boolean){
    this._isShow = value;
  }          
  public scrollSlideBody(event){
    if(event.target){
      if(event.target.scrollTop >= 10 && !this.isScroll){
        this.isScroll = true;
      }else if(event.target.scrollTop < 10 && this.isScroll){
        this.isScroll = false;
      }
    }
  }
}
