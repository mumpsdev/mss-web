import { Injectable, ElementRef, OnInit } from '@angular/core';
import { MenuRightComponent } from './menu-right/menu-right.component';
import { MenuLeftComponent } from './menu-left/menu-left.component';

@Injectable({
  providedIn: 'root'
})
export class MenuService  implements OnInit{
  public listMenu: Array<MenuRightComponent> = new Array();
  constructor(
    //   private elementRef: ElementRef
  ) { }

  ngOnInit(){
      
       
  }

  public addMenuRight(menu: any){
      this.listMenu.push(menu);
  }

  public closeAllMenuRight(menuAtual: any){
      this.listMenu.forEach(menu => {
          if(menu != menuAtual){
              menu._isShow = false;
          }
      })
  }

  public isMenuRightOpen(menuNow: any): boolean{
      let isOpen = this.listMenu.some(menu => 
          menuNow != menu && menu._isShow && menu.fixed
      );
      return isOpen;
  }
}
