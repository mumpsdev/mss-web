import {
   Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    Renderer2,
    ElementRef,
    ChangeDetectorRef,
    RendererStyleFlags2
  } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
import { MenuService } from '../menu.service';
import { ngIfSlideOutRight, ngIfSlideInRight, ngIfSlideInLeft, ngIfSlideOutleft } from '../../../utils/animates.custons';
import { RenderFlags } from '@angular/compiler/src/core';

@Component({
  selector: 'm-menu-right',
  templateUrl: './menu-right.component.html',
  styleUrls: ['./menu-right.component.scss'],
  animations: [ngIfSlideOutRight, ngIfSlideInRight, ngIfSlideInLeft, ngIfSlideOutleft]
})
export class MenuRightComponent implements OnInit {

  constructor(
    private renderer: Renderer2,
    private menuService: MenuService,
    private change: ChangeDetectorRef
  ) { 
    this.menuService.addMenuRight(this);
  }

  onInit = false;
  _isShow: boolean = false;
  @Input() titleClass: string;
  @Input() title: string;
  @Input() width: string;
  @Input() full: boolean;
  @Input() fixed: boolean = false;
  @Input() modal: boolean = false;
  position: string = "right";
  
  @Output() close: EventEmitter<any> = new EventEmitter();
  
  public isScroll: boolean = false;
  public animateContainer:any;
  public animateMove:any;
  
  ngOnInit() {
    if(!this.width){
      this.width="18rem";
    }
    this.menuService.closeAllMenuRight(null);
    this.onInit = true;
  }
  
  public closeClick(){
    this.isShow = false;
    this.close.emit(this.isShow);
  }

  @Input()
  set isShow(value: boolean){
    this.setValue(value);
    this.setPaddingPage(value);
  
  }

  get isShow(){
    return this._isShow;
  }

  setValue(value: boolean){
    let isMenuOpen = this.menuService.isMenuRightOpen(this);

    if(isMenuOpen){
      value = true;
    }else {
      if(this._isShow){
        value = false;
      }else if(this.onInit){
        value = true;
      }
    }
    this.menuService.closeAllMenuRight(null);
    this._isShow = value;
  }
          
   setPaddingPage(value: boolean){
    let isMenuOpen = this.menuService.isMenuRightOpen(null);
    let page =  document.querySelector(".page-root");
    console.log(isMenuOpen);
    if(isMenuOpen){
      this.renderer.setStyle(page, "max-width", `calc(100% - ${this.width})`);
    }else {
      this.renderer.setStyle(page, "max-width", "100%");
    }
  }

  public scrollSlideBody(event){
    if(event.target){
      if(event.target.scrollTop >= 10 && !this.isScroll){
        this.isScroll = true;
      }else if(event.target.scrollTop < 10 && this.isScroll){
        this.isScroll = false;
      }
    }
  }
}
