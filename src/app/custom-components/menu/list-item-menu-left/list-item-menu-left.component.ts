import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'm-list-item-menu-left',
  templateUrl: './list-item-menu-left.component.html',
  styleUrls: ['./list-item-menu-left.component.scss']
})
export class ListItemMenuLeftComponent implements OnInit {

  @Input() list: Array<any> = [];
  
  constructor(private router: Router) { }

  ngOnInit() {

  }


  public navegation(link: any){
    // this.close();
    this.list.filter( link => {
      if(link.links){
        link.links.filter( subLink => subLink["selected"] = false);
      }
      link["selected"] = false
    });
    link["selected"] = true;
    console.log(link);
    console.log(this.list);
    
    if(link.router){this.router.navigate([link.router]);}
  }

}
