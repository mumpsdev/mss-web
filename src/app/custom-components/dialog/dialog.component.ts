import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '../../utils/translate/translate.service';

@Component({
  selector: 'm-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<DialogComponent>,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    if(this.data.title) this.data.title = this.translateService.getValue(this.data.title);
    if(this.data.msg) this.data.msg = this.translateService.getValue(this.data.msg);
    if(!this.data.icon) this.data.icon = "warning";
    // if(!this.data.width) this.data.width = "30rem";
  }

  public onclickButton(button: any){
    if(button.action) button.action();
    if(button.close) this.dialogRef.close();
  }

}
